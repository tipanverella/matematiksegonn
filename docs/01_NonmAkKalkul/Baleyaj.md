# Approximation de $\sqrt{2}$

Si $a,b \ge 0$

$$
\begin{align*}
  a < b &\implies a\cdot a < a\cdot b\\
  &\implies a^2 < ab
\end{align*}
$$

men tou

$$
\begin{align*}
  a < b &\implies a\cdot b < b\cdot b\\
  &\implies ab < b^2
\end{align*}
$$

donk 

$$
\begin{align*}
  a^2 < ab < b^2 &\implies a^2 < b^2
\end{align*}
$$


```python
valeur = 1
pas = 0.1

while valeur**2 < 2:
  valeur = valeur + pas
  print(f"{valeur=}, {valeur**2=}")

print(f"{valeur - pas} < sqrt(2) < {valeur}")
print(f"{valeur =}")
```

    valeur=1.1, valeur**2=1.2100000000000002
    valeur=1.2000000000000002, valeur**2=1.4400000000000004
    valeur=1.3000000000000003, valeur**2=1.6900000000000006
    valeur=1.4000000000000004, valeur**2=1.960000000000001
    valeur=1.5000000000000004, valeur**2=2.2500000000000013
    1.4000000000000004 < sqrt(2) < 1.5000000000000004
    valeur =1.5000000000000004



```python
def aproksimasyon_rasin_2(pas):
  valeur = 1
  while valeur * valeur < 2:
    valeur = valeur + pas
  return valeur - pas, valeur
```


```python
aproksimasyon_rasin_2(0.000001)
```




    (1.414212999965924, 1.414213999965924)




```python
%timeit rasin_2 = aproksimasyon_rasin_2(0.1)
%timeit rasin_2 = aproksimasyon_rasin_2(0.001)
%timeit rasin_2 = aproksimasyon_rasin_2(0.00001)
%timeit rasin_2 = aproksimasyon_rasin_2(0.0000001)
```

    915 ns ± 302 ns per loop (mean ± std. dev. of 7 runs, 1000000 loops each)
    42.3 µs ± 827 ns per loop (mean ± std. dev. of 7 runs, 10000 loops each)
    4.12 ms ± 25.8 µs per loop (mean ± std. dev. of 7 runs, 100 loops each)
    415 ms ± 6.6 ms per loop (mean ± std. dev. of 7 runs, 1 loop each)



```python
def aproksimasyon_rasin_2_compteur(pas):
  valeur = 1
  compteur = 0
  while valeur * valeur < 2:
    valeur = valeur + pas
    compteur = compteur + 1
  return valeur - pas, valeur, compteur
```


```python
aproksimasyon_rasin_2_compteur(0.1)
```




    (1.4000000000000004, 1.5000000000000004, 5)




```python
aproksimasyon_rasin_2_compteur(0.01)
```




    (1.4100000000000004, 1.4200000000000004, 42)




```python
aproksimasyon_rasin_2_compteur(0.001)
```




    (1.4139999999999544, 1.4149999999999543, 415)




```python
aproksimasyon_rasin_2_compteur(0.0001)
```




    (1.4141999999999544, 1.4142999999999544, 4143)




```python
aproksimasyon_rasin_2_compteur(0.00001)
```




    (1.4142100000027136, 1.4142200000027136, 41422)




```python
aproksimasyon_rasin_2_compteur(0.000001)
```




    (1.414212999965924, 1.414213999965924, 414214)




```python
def aproksimasyon_rasin_2_compteur(pas, commencer):
  valeur = commencer
  compteur = 0
  while valeur * valeur < 2:
    valeur = valeur + pas
    compteur = compteur + 1
  return valeur - pas, valeur, compteur
```


```python
aproksimasyon_rasin_2_compteur(0.1,1)
```




    (1.4000000000000004, 1.5000000000000004, 5)




```python
aproksimasyon_rasin_2_compteur(0.01,1.4)
```




    (1.41, 1.42, 2)




```python
aproksimasyon_rasin_2_compteur(0.001,1.41)
```




    (1.4139999999999995, 1.4149999999999994, 5)




```python
aproksimasyon_rasin_2_compteur(0.0001,1.4139999999999995)
```




    (1.4141999999999995, 1.4142999999999994, 3)




```python
aproksimasyon_rasin_2_compteur(0.00001,1.4141999999999995)
```




    (1.4142099999999995, 1.4142199999999996, 2)


