# [Rapel Trigonometri](https://en-m-wikipedia-org.translate.goog/wiki/Trigonometry?_x_tr_sl=en&_x_tr_tl=fr&_x_tr_hl=en&_x_tr_pto=wapp)
![](https://lh3.googleusercontent.com/pw/AMWts8DG0TIT46UNAQZwV18yvxncqcDnGniTlkdxVvlrI_fayleZW5MyBCKlEggE8VCPQD1gTVOVMumCAP4GlamaJKbGpDzUJC_VI50sYODlbqGRBjRUGUMm0AgumvLh97ZJPHt4u1KdL9KLUhZeFAFB2eBt-w=w765-h1020-no?authuser=0)
![](RapelTrigonometri.gif)


# Question 3

## Traduire $\delta > \alpha$ en fraction:

![](AdditionDuCancre.png)

$$
\begin{align*}
\alpha < \delta &\iff\tan \alpha < \tan \delta, \quad\text{paske tanjant se yon fonksyon monoton}\\
&\implies \frac{a}{b} < \frac{a+c}{b+d}
\end{align*}
$$

## Demontrer que $\delta < \beta$:

Nou pral montre ke $\beta - \delta > 0$.
Sonje ke: 

$\beta - \delta > 0 \iff \tan \beta -\tan \delta > 0$.

Nap itilize teknik anle a pou nou ekri tanjant yo an fraksyon:

$$ 
\begin{align*}
\tan\beta - \tan\delta &= \underbrace{\frac{c}{d}}_{\tan \beta} - \underbrace{\frac{a+c}{b+d}}_{\tan \delta} \\
&\\
&= \frac{bc+cd-ad-cd}{(b+d)d}, \quad cd -cd = 0\\
&\\
&= \frac{bc-ad}{(b+d)d}
\end{align*}
$$

$a,b,c,d > 0 \implies (b+d)d > 0$.  Egal $\tan \beta - \tan \delta$ gen menm siy ak $bc - ad$.  

### Ki siy $bc-ad$?
Sonje ke yo di nou ke $\displaystyle \frac{c}{d} > \frac{a}{b}$:

$$
\begin{align*}
\frac{c}{d} > \frac{a}{b} &\implies c > \frac{ad}{b}, \quad \text{mwen multipliye tout bagay pa } c\\
&\implies bc > ad, \quad \text{mwen multipliye tout bagay pa }b\\
&\implies bc - ad > 0.
\end{align*}
$$

Donk $bc -ad$ pozitif, ki vle di ke:

$$
\begin{align*} 
\frac{bc -ad}{(b+d)d} > 0 &\implies \tan \beta - \tan \delta > 0 \\
&\iff \beta - \delta > 0\\
&\iff \beta > \delta
\end{align*}
$$