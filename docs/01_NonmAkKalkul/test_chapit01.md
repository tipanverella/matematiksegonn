# Test pou Chapit 1 (san kalkulatris)

## 1. Sismoloji

Echèl grandè moman an ( MMS ; ke nou ekri $M_w$) se yon mezi grandè tranblemanntè a ("gwosè" oswa fòs) ki baze sou moman sismik li.  Li te defini nan yon papye 1979 pa Thomas C. Hanks ak Hiroo Kanamori . Menm jan ak mayitid lokal/echèl Richter ($M_L$) defini pa Charles Francis Richter an 1935, li itilize yon echèl logaritmik; ti tranblemanntè gen apeprè menm grandè sou toude echèl yo. Malgre diferans lan, medya nouvèl yo souvan di "echèl Richter" lè yo refere a echèl grandè moman an.


|$M_w$| Enèji (joul) | Ekivalans an Bonm Hiroshima a|
|---|---|---|
|4|$6.3\times 10^{10}$|$0.0012$|
|5|$2.0\times 10^{12}$|$0.038$|
|6|$6.3\times 10^{13}$|$1.2$|
|7|$2.0\times 10^{15}$|$38$|
|8|$6.3\times 10^{16}$|$1200$|
|9|$2.0\times 10^{18}$|$38000$|

- (a) Tranblemanntè 12 Janvye 2010 an Ayiti a te $M_w = 7.0$; a konbyen bonm Hiroshima sa ekivo?

- (b) Ekri yon fraksyon senplifye ki di konbyen fwa yon tranblemanntè $M_w=6.0$ li te ye? (konpare Enèji yo).

- (c) Pi gro tranblemanntè ke nou dokumante, le 22 Me 1960 an Valdivia o Chili, te gen yon $M_w=9.5$; aproximativman konbyen fwa tranblemanntè 12 Janvye 2010 an Ayiti li te ye?


## 2. Rezoud Inegalite sa yo:

- (a) $\quad \displaystyle \frac{9}{4}x + \frac{6}{7} > 4$

- (b) $\quad \displaystyle 6-\frac{17}{3}x \le -\frac{5}{3}$

- (c) $\quad \displaystyle \frac{8+x}{7} < 2x+\frac{9}{14}$


## 3. Konplete tablo sa a ak valè $f(x)$ (an fraksyon senplifye) pou chak valè $x$ yo:

$$
\begin{align*}
    f(x) = \frac{2x}{x^2 + 1}
\end{align*}
$$

|$x$|$-5$|$-1$|$4$|$8$|$10$|
|:---:|---|---|---|---|---|
|$f(x)$|

## 4. De ti kalkul an plus pou nou fini:

- (a) Ekri avèk valè absolu inegalite sa a: $\quad \displaystyle -\frac{2}{3} > x > -\frac{6}{7}$

- (b) Senplifye: $\quad \displaystyle \frac{\sqrt{3}}{2 - \sqrt{5}}$
