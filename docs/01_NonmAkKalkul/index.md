## Plan Travay pou Chapit 01, Semenn 1

|Jou|Mr. Delia (30mins)| Tipan (90mins)| Devwa Direksyon| Devwa Weekend|
|:---:|-------|------|----------------|--------------|
|Lendi|1,2,3,4,5 |[Baleyaj](Baleyaj.md)     |[80,82](https://lh3.googleusercontent.com/pw/AMWts8DQJhxCY_bFqdxDFzgm0H2uJb3BBmBPJpSityB7KLgfTxKR2wW2Y53UwsHLmZ1eJ8fjqVUPZtZFYr8gOtDH_nAEfHs94DVn-WyrpLLbHhVW0EPqn81evYzPSV1qEbLncBKeWdRq9q4KvpFmBGrZS4HrDw=w765-h1020-no?authuser=0)||
|Madi |6,7,8,9,10 |Korelasyon nan echel diferan      |88,105||
|Jedi |11,12,13 |[L'addition du cancre](AdditionDuCancre.md)      |114|[137, 138,141](https://lh3.googleusercontent.com/pw/AMWts8D9HcoDqPr9JQ2znTktALwHVPKYGThLQAER6Pz2PvC724QM5hmVU-rFilGB07pjm6dk0TJJEXA1D22MvmjHAzqHFEF8IQ8LZKVJLuQ51Wliae0og9AEZiFJ9llK8VF5ID3zuwHIhXiAfBrmsfyIpV-JIQ=w1990-h914-no?authuser=0)|

## Plan Travay pou Chapit 01, Semenn 2

|Jou|Mr. Delia (30mins)| Tipan (90mins)| Devwa Direksyon| Devwa Weekend|
|:---:|-------|------|----------------|--------------|
|Lendi|Koreksyon: 137, 138,141|Decimal Etonnant      |49,54||
|Madi |6 calculs|[Revizyon](https://lh3.googleusercontent.com/yUDNpfXxGpd6JnZ05rch7AVNLSTmWImnkokzF6x_Baj6drtyyzT5E5KpOm_oiX7VL4-3f9-xOh3ohctzRdbuyyZPCJS0ljc-tZMewF8V0wiPcJmgcmxwDs3Om6cQ9lpGWEd-l3g39knkMz33EJkCBlxCSguicONl2k0QZ0pGjyts7mfFYhyFJ_L2xepbt5xQ_cYBk9ALGw8NojVJL9GOsvbJ-X4GRHBlBKq75Az22hQ_bEv9vTgYtR9FdKN4_wcVY_sHKwf2XenwVtCnk6-BXHSNq5wvt52yV-Ehv-X4k7F2NI2gVxnRokjutRADRD_Lja4CWSVIDXMosRPLrHQ1jykA0dUdoTg0Sbbq52QWi_EE67tK-nH_0rm8KuLebTnhdS3CZvuEejOsUuo-pJ64lgmM5DPALYXlyaedfQImheme2Jv2ZD8ZKEC9RNCyUxmgf2MM9llsyaeLQOBRfPgykMb7opIetdsMFWnbyiTs-GY-R6EMqT0wFfQbHoZlFHrbwN1ZUh-eqeTPRf6o1ojuugiRAjdE_KiKYmZB0ID_7BmvNGXtb_4M996d98s7AmwNb1qNunkr_Tuefm493YoT9cJMl31P3SbZdd_AlcbBdcs-Bc3N1_EgtZnWLFxfL2pQ3KzyHRR8_oaQuyLu2drWNLaaBhBeX5dRK40sAEph7bNqcMClRjyNyK6ls2ndb8Y895Udq377gS60m27mEp0RajOgi-ljQGc7rFjm-VfjIp25mUgrqFYEX6VaNp_00J-goXWcSAYMsQqczYBnjPIo1DPdV7Ezh16F8PnFlzbTLH_sAwBgPJVkrSAdf897Esh2U2VMUGVRP2Xnx1XEf_Zkes5EAOa0bDxYQIBQ4hPPByCxMs6k9HUCul8gzJNzNoRsNvXj8D5COMBrp6wSkFl_kRte3Uvh4YyQc6faBrj0TlsoBq9F-Q=w821-h1020-no?authuser=0), [Bilan8](Bilan8.gif), [logaritm](https://photos.google.com/share/AF1QipOqvEGWrDa454QB4iRHngi82tWV6DY22bmOigID0etOOUo7b0uPjaOG7gee9fKeUA/photo/AF1QipNfsLJDUvA3eZpd-1Ha49EoZ-oKNAEc_WOSy6Se?key=ZmxOSHNTZGh3UWtnamVRV1I3aHFlbjhtTE8xN1h3), [tranblemandete](https://en-m-wikipedia-org.translate.goog/wiki/Richter_magnitude_scale?_x_tr_sl=en&_x_tr_tl=fr&_x_tr_hl=en&_x_tr_pto=wapp)       |63,68||
|Jedi |[Test (40 a 60 mins)](test_chapit01.md)|[Koreksyon](https://photos.google.com/share/AF1QipOqvEGWrDa454QB4iRHngi82tWV6DY22bmOigID0etOOUo7b0uPjaOG7gee9fKeUA/photo/AF1QipOAFUqE6ynILp_cExD5-3hb3gyaNi2J6Sb4OUWi?key=ZmxOSHNTZGh3UWtnamVRV1I3aHFlbjhtTE8xN1h3)      |155|Li section Activites, Cours et Methodes prochen chapit lan|


## Plan Travay pou Chapit 02, Semenn 1

|Jou|Chofe (30mins)| (90mins)| Devwa Direksyon| Devwa Weekend|
|:---:|-------|------|----------------|--------------|
|Lendi|3,5 | [multip, divize, nonm premye, parite](https://photos.google.com/share/AF1QipOqvEGWrDa454QB4iRHngi82tWV6DY22bmOigID0etOOUo7b0uPjaOG7gee9fKeUA/photo/AF1QipOkFygoG1SXt65TCQGKVc1I_eLqiHhkU10A7I_H?key=ZmxOSHNTZGh3UWtnamVRV1I3aHFlbjhtTE8xN1h3)    |44,47||
|Madi |59 |[parite ak nonm premye](https://photos.google.com/share/AF1QipOqvEGWrDa454QB4iRHngi82tWV6DY22bmOigID0etOOUo7b0uPjaOG7gee9fKeUA/photo/AF1QipM7-a_bc-vMRMapXuP72EAyqFI93Gxk3vnETwxG?key=ZmxOSHNTZGh3UWtnamVRV1I3aHFlbjhtTE8xN1h3)      |55||
|Mekredi|      |TP (paj 59) |  ||
|Jedi   |13,15 |Ti Teorem Fermat |58| paj 59 pati C, paj 60 pati C|