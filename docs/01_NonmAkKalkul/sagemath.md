# Nonm Reyel ak Inegalite


```python
I = RealSet(-oo, pi)
J = RealSet.closed_open(sqrt(2), sqrt(3))
K = RealSet(-oo, oo)
```


```python
I
```




    (-oo, pi)




```python
0 in I
```




    True




```python
3 in I
```




    True




```python
-5 in I
```




    True




```python
3.14 in I
```




    True




```python
12.5 in I
```




    False




```python
J
```




    [sqrt(2), sqrt(3))




```python
sqrt(2) in J, sqrt(3) in J
```




    (True, False)




```python
1.5 in J
```




    True




```python
3 in J
```




    False




```python
K
```




    (-oo, +oo)




```python
3 in K
```




    True




```python

```
