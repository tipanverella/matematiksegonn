# Nomm Ak Kalkul

Gen plizye kategori de nonm an matematik. 


## Poukisa $\sqrt{2} \notin \mathbb{Q}$ ?
![](https://lh3.googleusercontent.com/pw/AL9nZEU3BsQodrK0AInqm8hpwoJe5UBP2ITbCcNUksBZu2EDSRWlsDGo4xqRyr-q6shFelOFUCp7ERoQF57Ml2cmDKUhbdw6HQG0ZRhf-W5rOa2amDYsFIioXX8jM-zBIXUqsJCypmzznFLKoxdUx63_4ZMFOQ=w1360-h1021-no?authuser=0)
## Ansanm Reyel yo $\mathbb{R}$

- $\mathbb{R}$ se yon chan:
  - sa vle di li se yon group pou adisyon, 
  - e li se yon group pou  multiplikasyon
  - e multiplikasyon distribuye nan adisyon
- $\displaystyle \mathbb{R} = \left\{-10, \ldots, -1, \ldots, 0, \ldots, \sqrt{2}, \ldots, \pi, \ldots,\frac{22}{7} \right\}$

## Utilize dwat Reyel yo
![](https://lh3.googleusercontent.com/pw/AL9nZEU88_6lsZjFq8kJ-QJrhE_hLpfsHIFvUSVuaey9TFv_w5F6J18qKb8MDrL1PsXmTeCWEExpQ9LG70o5TU6Qx_6qrHpYeauy24YHUgPrKCF4b1yo1jn0pvAm67bfcls9zFITrYN7LBFoh0wky6wz6pMyBw=w1360-h1021-no?authuser=0)

## Reprezante yon Enteval


![](https://lh3.googleusercontent.com/pw/AL9nZEXRlGCQzqHCDZQ7pdT8CgBkOTGO4rZ45fruZp9y8jQSUQdbplJs4Qug-wMk5aDPpizMym0hUPrFBSJhZSZrId1CuKc60Pp-9UXPRsKJcS_a038v_OtCRg4InBPbyY8gxBhci_mW8AUw6UkZ29m2KyBjgw=w1360-h1021-no?authuser=0)

![](https://lh3.googleusercontent.com/pw/AL9nZEVycWie9tCCtcK8i1AkONN_DMJJIibAJzr3iedTLvIRGbFyu6TfQb6Ymu_j2CZGLWRJ3IMNv2v-iV2lYQbVEkUV1FUqQP7j4cB4kCIYljql8lC1QBxQXjXTqS112sK5lT29wGlXWbuKBg37VCVRHO_cNg=w1360-h1021-no?authuser=0)

![](https://lh3.googleusercontent.com/pw/AL9nZEWaAQYjBGXSAUIoi6ciLcndPRWos3YTLN_jPSDybihlw2YWmQTqtKbirZVdlUR6V6xD17_KnsAJFZOVB9pvRXtsY3xkRdfbi8jgEXYSm3300xIf7G8RygATdwsPHojdqw0RNlWHzGRtgGhUBAoK4vdd6Q=w1360-h1021-no?authuser=0)
