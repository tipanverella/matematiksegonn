# Bonjou

Mwen kontan akeyi nou nan kou Matematik Segonn lan pou Ane 2023 a!

|                      Men rele Tipan!                       |
| :--------------------------------------------------------: |
| ![](https://tipanverella.github.io/images/tipan_child.jpg) |

Mwen ap fasilite aprantisaj nou de materyel lan ak ed Dede!

## [Silabus](https://fr.wikipedia.org/wiki/Syllabus_(enseignement)) lan

- [Nonm Reyel ak Kalkul](01_NonmAkKalkul/index.md)
- [Jeometri](02_Jeometri/index.md)
- [Fonksyon](03_Fonksyon/index.md)
- [Statistik ak Probabilite](04_StatistikAkProbabilite/index.md)


