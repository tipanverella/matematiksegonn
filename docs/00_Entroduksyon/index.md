# Kisa ki enpotan lan Matematik?

- Definisyon
- Abstraksyon
- Schema
- Simetri
- Enfini


# Propiyete Nonm yo
## Antye Naturel, $\mathbb{N}$

$$
\mathbb{N} = \{0,1,2,3,4,5,6,7,8,9,10,11, \ldots, 8758579, \ldots \}
$$

- Tankou $0, 1, 2, 3, \ldots, 673 029 865 234, \ldots$
- Nonm sa yo ou ka adisyone yo, e multipliye yo
- Men ou pa toujou ka soustre yo ou byen divize yo


## Ekritu nou de nonm yo fet an [baz](https://en-m-wikipedia-org.translate.goog/wiki/Radix?_x_tr_sl=en&_x_tr_tl=fr&_x_tr_hl=en&_x_tr_pto=wapp)

Nou ekri an baz **10**:

$$
\begin{align*}
    327 &= \underbrace{7}_{\text{unite}} + \underbrace{2}_{\text{dizenn}} \cdot 10 + \underbrace{3}_{\text{santenn}} \cdot 100  \\
    &= 7\cdot{10^0} + 2\cdot10^1 + 3\cdot 10^2
\end{align*}
$$

Men nou te ka ekri tou an baz **8**:

$$
\begin{align*}
    123_8 &= \underbrace{3}_{\text{unite}} + \underbrace{2}_{\text{witenn}} \cdot 8 + \underbrace{1}_{\text{swasannkatenn}} \cdot 64  \\
    &= 3\cdot{8^0} + 2\cdot8^1 + 1\cdot 8^2\\
    &=83_{10}
\end{align*}
$$

Oubyen an baz **5**:

$$
\begin{align*}
    342_8 &= \underbrace{2}_{\text{unite}} + \underbrace{4}_{\text{senktenn}} \cdot 5 + \underbrace{3}_{\text{vennsenktenn}} \cdot 25  \\
    &= 2\cdot{5^0} + 4\cdot5^1 + 3\cdot 5^2\\
    &=97_{10}
\end{align*}
$$

Men baz ki pi kouran apa de baz 10, se baz **2**:

$$
\begin{align*}
10011_2 &= 1\cdot2^0 + 1\cdot 2^1 + 0\cdot2^2 + 0\cdot2^3 + 1\cdot2^4\\
	&= 1 + 2 + 0 + 0 + 16\\
	&= 19_{10}
\end{align*}
$$

Ou gendwa utilize [base-converter](https://www.rapidtables.com/convert/number/base-converter.html) pou verifye kalkul ou!

## Antye yo $\mathbb{Z}$

- Tankou $0, -1, 2, 3, \ldots, -29 865 234, \ldots$
- Nonm sa yo ou ka adisyone yo, multipliye yo, e soustre yo
- Men ou pa toujou ka divize yo

## Nonm Rasyonel yo $\mathbb{Q}$

- Tankou $0, -1, 2, 3, \ldots, -29 865 234, \ldots$
- Nonm sa yo ou ka adisyone yo, multipliye yo, soustre yo, e divize yo


## Nosyon Group lan

## Nosyon de Chan an, e propriete nonm rasyonel yo


# Kouman ou Rezoud yon Inegalite

## Youn ki pa tro konplike

Pou vreman konnen si ou konn rezoud ekwasyon, fok ou eseye rezoud yon inegalite!

An nou konsidere inegalite $2x-4 > 5$ lan.
Pou nou rezoud li, fok nou izole $x$.  
Vesyon kou la se:

$$
\begin{align*}
2x -4 &> 5\\
2x &> 9\\
x&>\frac{9}{2}.
\end{align*}
$$

Men an nou itilize propriete nonm rasyonel yo pou nou fe sa:

$$
\begin{align*}
2x -4 &> 5\\
2x-4 + \underbrace{4}_{\text{paske se simetrik nan adisyon de }-4} &> 5 + \underbrace{4}_{\text{paske fok mwen pa modifye inegalite a}}\\
2x &> 9\\
\underbrace{\frac{1}{2}}_{\text{paske se simetrik nan multiplikasyon de }2, \text{ e ke li pozitif}}\cdot 2x &> \underbrace{\frac{1}{2}}_{\text{paske mwen pa dwe modifye inegalite a}} \cdot 9\\
x&>\frac{9}{2}.
\end{align*}
$$

## Poukisa siy multiplikate a change sans inegalite a?
Bagay yo tap on ti jan pi subtil, si fok nou te rezoud, pa egzanp, $-x > 7$.  Refleks nou se tap:

$$
\begin{align*}
    -x > 7 &\implies -1\cdot -x \underbrace{<}_{\text{siy inegalite a change paske nou multipliye ak yon nonm negatif}} -1\cdot{7}\\
    &\implies x < -7.
\end{align*}
$$

Men gen yon lot jan pou nou konprann sa kap pase a:

$$
\begin{align*}
    -x > 7 &\implies -x + x > 7 + x\\
    &\implies 0 > 7 + x\\
    &\implies -7 + 0 > -7 + 7 + x\\
    &\implies -7 > x\\
    &\implies x < -7.
\end{align*}
$$

## Kouman pou rezoud yon problem tankou: $\displaystyle \frac{7x-5}{6x+3} > 4$ ?

### A) Ak Lojik:

1. Fok ou eklu tout solusyon kote $6x+3 = 0$
2. Fok ou rezoud problem 2 fwa, 
    - youn pou si $6x+3 > 0$, 
    - e yon lot pou si $6x+3 < 0$

```mermaid
---
title: Rezoud Inegalite
---
flowchart TB
    deno{6x+3, positif};
    deno-->|wi: x > -1/2|positif[rezoud, 7x-5 > 24x+12];
    deno-->|non: x < -1/2|negatif[rezoud, 7x-5 < 24x+12];
    positif-->pos[x < -1, e x > -1/2];
    negatif-->neg[x > -1, e x < -1/2];
    pos --> desizyon{kiyes ki fe sans};
    neg --> desizyon;
    desizyon-->fini[se: x > -1, e x <> -1/2, paske -1/2 > -1];
```
Donk solusyon an se $x \in \quad ]-1, -\frac{1}{2}[$.


### B) Ak yon tablo syi:

1. Fok ou eklu tout solusyon kote $6x+3 = 0$
2. Re-ekri inegalite a: $\displaystyle \frac{7x-5}{6x+3} > 4 \implies \frac{7x-5}{6x+3} - 4 > 0 \implies \frac{-17x-17}{6x+3} > 0$.
3. Sonje ke yon kosyan positif si numerate li ak denominate li gen menm 
siy:

    |fonksyon $x$ yo| $\quad ]-\infty,-1[$ | $\quad ]-1,-\frac{1}{2}[$| $\quad ]-\frac{1}{2},\infty[$|
    |:---:|:---:|:---:|:---:|
    |$-17x-17$|+|-|-|
    |$6x+3$|-|-|+|
    |$\displaystyle \frac{-17x-17}{6x+3}$|-|+|-|

Donk solusyon an se $x \in \quad ]-1,-\frac{1}{2}[$.


|    De dwat yo: <span style="color:blue">$y=6x+3$</span> ak <span style="color:red">$y=-17x-17$</span>                     |
| :--------------------------------------------------------: |
| ![](de_dwat_yo.png) |


## Yon ti rapel: Rol Koefisyan Direkte ak Odone lan Orijin

Lan animasyon ki anba yo: 

- Ekwasyon dwat la se toujou $y = a\cdot x +b$.
- Pwen $A = (-1, -a +b)$, ak pwen $B = (1, a + b)$ yo toujou sou dwat la, e absis yo toujou fix (se odone yo selman ki chanje).  Nou mete yo pou ede ou we kijan dwat lan chanje.

|    Le nou chanje $a$, Koefisyan Direkte a| Le nou chanje $b$, Odone lan Orijin lan|
| :---: | :---: |
| ![](DwatAfin_a.gif) | ![](DwatAfin_b.gif) |
