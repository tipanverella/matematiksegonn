Nap suiv [lyen sa'a](https://docs.python.org/fr/3/tutorial/index.html)


```python
# yon deklarasyon
spam = 1
```


```python
spam
```




    1




```python
# yon deklarasyon teks
text = "# This is not a comment because it's inside quotes."
```


```python
text
```




    "# This is not a comment because it's inside quotes."




```python
2 + 2
```




    4




```python
50 - 5 *6
```




    20




```python
(50 - 5*6) / 4
```




    5.0




```python
17 / 3 
```




    5.666666666666667




```python
17 // 3
```




    5




```python
17 % 3 
```




    2




```python
2 ** 7
```




    128




```python
n
```


    ---------------------------------------------------------------------------

    NameError                                 Traceback (most recent call last)

    <ipython-input-13-ab0680a89434> in <module>
    ----> 1 n
    

    NameError: name 'n' is not defined



```python
n = 357
```


```python
n
```




    357




```python
4 * 3.758 - 1
```




    14.032




```python
gro_nonm = 3_498_656_534
```


```python
gro_nonm
```




    3498656534




```python
round(4 * 3.758 - 1, 2)
```




    14.03




```python
int(round(4 * 3.758 - 1, 0))
```




    14




```python
non_pam = "Tipan\nVerella"
```


```python
print(non_pam)
```

    Tipan
    Verella



```python
text = "
Ou mande m cheri, kisa pwezi vle di
Enben yon bann ti fraz, nou makonen ansanm
Pou chavire male
"
```


      File "<ipython-input-29-36b5d3569cc3>", line 1
        text = "
                ^
    SyntaxError: EOL while scanning string literal




```python
text = """
Ou mande m cheri, kisa pwezi vle di
Enben yon bann ti fraz, nou makonen ansanm
Pou chavire male
"""
```


```python
print(text)
```

    
    Ou mande m cheri, kisa pwezi vle di
    Enben yon bann ti fraz, nou makonen ansanm
    Pou chavire male
    



```python
non_pam = "Sameena Mulla"
laj_mwen = 45
```


```python
msg = f"Moun ki rele {non_pam} a, gen {laj_mwen} lane!"
print(msg)
```

    Moun ki rele Sameena Mulla a, gen 45 lane!


# Chenn karaktek yo se de iterab


```python
for _bagay in non_pam:
  print(_bagay)
```

    S
    a
    m
    e
    e
    n
    a
     
    M
    u
    l
    l
    a



```python
for karakte in laj_mwen:
  print(karakte)
```


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    <ipython-input-39-9cb6a50d2234> in <module>
    ----> 1 for karakte in laj_mwen:
          2   print(karakte)


    TypeError: 'int' object is not iterable



```python
non_pam[0], non_pam[-1], non_pam[3:7]
```




    ('S', 'a', 'eena')



# Nou ap koumanse sou list apre sa


```python

```
