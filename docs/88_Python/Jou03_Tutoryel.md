```python
s = 'First line.\nSecond line.'
print(s)
```

    First line.
    Second line.



```python
print("""\
Usage: thingy [OPTIONS]
     -h                        Display this usage message
     -H hostname               Hostname to connect to
""")
```

    Usage: thingy [OPTIONS]
         -h                        Display this usage message
         -H hostname               Hostname to connect to
    



```python
text = (
    'Put several strings within parentheses '
    'to have them joined together.'
    ' Mwen di sa mwen di a!'
    )
print(text)
```

    Put several strings within parentheses to have them joined together. Mwen di sa mwen di a!



```python
3 * 'un' + 'ium'
```




    'unununium'




```python
3 * ('un' + 'ium')
```




    'uniumuniumunium'




```python
3*'un' + 3*'ium'
```




    'unununiumiumium'




```python
3 * ('un' + 'ium') == 3*'un' + 3*'ium'
```




    False




```python
# Komutativite
'un' + 'ium' == 'ium' + 'un'
print(f"{'un' + 'ium' =}")
print(f"{'ium' + 'un'=}")
```

    'un' + 'ium' ='unium'
    'ium' + 'un'='iumun'



```python
nonm = "Tipan"
print(f"{nonm[0] = }")
print(f"{nonm[-1] = }")
print(f"{nonm[1] = }")
print(f"{nonm[3] = }")
print(f"{nonm[4] = }")
```

    nonm[0] = 'T'
    nonm[-1] = 'n'
    nonm[1] = 'i'
    nonm[3] = 'a'
    nonm[4] = 'n'



```python
wife = "Sameena"
print(f"{wife[0] = }")
print(f"{wife[-1] = }")
print(f"{wife[1] = }")
print(f"{wife[3] = }")
print(f"{wife[4] = }")
```

    wife[0] = 'S'
    wife[-1] = 'a'
    wife[1] = 'a'
    wife[3] = 'e'
    wife[4] = 'e'



```python
print(f"{wife[-2] = }")
```

    wife[-2] = 'n'



```python
print(f"{wife=}")
print(f"{wife[2:6]=}")
```

    wife='Sameena'
    wife[2:6]='meen'



```python
kare = [1, 4, 9, 16, 25, 36, 49, 64]
print(f"{kare=}")
print(f"{kare[2:6]=}")
print(f"{kare[0] = }")
print(f"{kare[-1] = }")
print(f"{kare[1] = }")
print(f"{kare[3] = }")
print(f"{kare[4] = }")
```

    kare=[1, 4, 9, 16, 25, 36, 49, 64]
    kare[2:6]=[9, 16, 25, 36]
    kare[0] = 1
    kare[-1] = 64
    kare[1] = 4
    kare[3] = 16
    kare[4] = 25



```python
kare + kare
```




    [1, 4, 9, 16, 25, 36, 49, 64, 1, 4, 9, 16, 25, 36, 49, 64]




```python
2 * kare
```




    [1, 4, 9, 16, 25, 36, 49, 64, 1, 4, 9, 16, 25, 36, 49, 64]




```python
care = [1, 4, 9, 16, 25, 36, 49, 64, 81]
```


```python
care == kare
```




    False




```python
ell = [1,2,3,"Tipan", [4,5,6], {1,2, "Sameena"}, -17.4]
```


```python
print(f"{ell=}")
print(f"{ell[2:6]=}")
print(f"{ell[0] = }")
print(f"{ell[-1] = }")
print(f"{ell[1] = }")
print(f"{ell[3] = }")
print(f"{ell[4] = }")
```

    ell=[1, 2, 3, 'Tipan', [4, 5, 6], {1, 2, 'Sameena'}, -17.4]
    ell[2:6]=[3, 'Tipan', [4, 5, 6], {1, 2, 'Sameena'}]
    ell[0] = 1
    ell[-1] = -17.4
    ell[1] = 2
    ell[3] = 'Tipan'
    ell[4] = [4, 5, 6]



```python

```
