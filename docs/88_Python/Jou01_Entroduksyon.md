```python
1+1
```




    2



Aparamman, `Python` pa pi **sot** ke sa!

# Entet

## Sou tit

- al li sou [Markdown](https://www.markdownguide.org/cheat-sheet/)
- al li sou [Python, Wikipedia](https://en.wikipedia.org/wiki/Python_(programming_language))
- al li [Tutoriel Python](https://docs.python.org/3/tutorial/index.html) an

![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Coat_of_arms_of_Haiti.svg/1280px-Coat_of_arms_of_Haiti.svg.png)

`Python: 1+1 = 2`

```python
for _ in range(10):
  print("Bonjou Tout Moun!")
```

$$
x + y = z
$$


```python
1+2+3
```




    6




```python
5*4*3*2*1
```




    120



## Routin:

```python
def <non fonksyon an>(<paramet fonksyon an>):
  ...
  return <reponse>
```

pa egzanp,
```python
def ajoute2(x):
  return x+2
```  


```python
x = 2
y = 3

x+y - x*y
```




    -1




```python
def fonksyon_01(x, y):
  return x+y - x*y
```


```python
fonksyon_01(2,3)
```




    -1




```python
fonksyon_01(4,5)
```




    -11




```python
fonksyon_01(3,2)
```




    -1




```python
fonksyon_01(-2,3)
```




    7



## Iterasyon


```python
for _x in range(1,13):
  print(f"pou x = {_x}, {fonksyon_01(_x, -3)}")
```

    pou x = 1, 1
    pou x = 2, 5
    pou x = 3, 9
    pou x = 4, 13
    pou x = 5, 17
    pou x = 6, 21
    pou x = 7, 25
    pou x = 8, 29
    pou x = 9, 33
    pou x = 10, 37
    pou x = 11, 41
    pou x = 12, 45



```python

```
