test: clean
	poetry run mkdocs build --strict --verbose --site-dir test

build: clean
	poetry run mkdocs build --strict --verbose	

clean:
	rm -rf test public

.PHONY: clean